from scrapy import Request, Spider


class CreepSpider(Spider):
    name = 'creepSpider'
    start_urls = [
        'https://bluemo4.xyz/category/250-%d8%b3%d8%b1%db%8c%d8%a7%d9%84-%d8%a8%d8%b1%d8%aa%d8%b1-imdb/',
        'https://bluemo4.xyz/category/%d8%a8%d8%b1%d8%aa%d8%b1%db%8c%d9%86-%d9%87%d8%a7%db%8c-imdb/',
    ]

    def parse(self, response, **kwargs):
        for data in response.css('article.postItems'):
            yield {
                'title': data.css('div.post-title h2 a::text').get(),
                'imdb rating': data.css('div.imdb-rating span.text-warning::text').get(),
                'tags': data.css('a[rel="tag"]::text').getall(),
                'story': data.css('div.pstory p::text').get(),
            }

        next_page = response.css('a[class="next page-numbers"]::attr(href)').get()
        if next_page:
            next_page = response.urljoin(next_page)
            yield Request(url=next_page, callback=self.parse)
