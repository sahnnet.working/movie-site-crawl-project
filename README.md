# Movie site crawl project

## Table of Content

1. [Installation](#installation)
2. [Quick start](#quick-start)
    * [Run Code](#run-code)
    * [Save Output](#save-output)

___

### Installation

```
pip install -r requirements.txt 
```

### Quick Start

#### run code

in directory : \movie-site-crawl-project\crawl>

```bash
scrapy crawl creepSpider
```

#### save output

```bash
scrapy crawl creepSpider -o info.[jl,json,csv]
```